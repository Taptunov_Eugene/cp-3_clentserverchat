import {
  APP_FINISH_INIT,
  USER_GET_SUCCESS,
  USERS_GET_SUCCESS,
  ROOMS_GET_SUCCESS,
} from './constants';

export const initialState = {
  isReady: false,
  currentUser: {},
  users: [],
  rooms: [],
  currentRoomMessages: [],
  onlineUsers: [],
};

export const appReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case APP_FINISH_INIT: {
      return { ...state, isReady: true };
    }
    case USER_GET_SUCCESS: {
      return { ...state, currentUser: payload.currentUser };
    }
    case USERS_GET_SUCCESS: {
      return { ...state, users: payload.users };
    }
    case ROOMS_GET_SUCCESS: {
      return { ...state, rooms: payload.rooms };
    }
    default: {
      return state;
    }
  }
};
