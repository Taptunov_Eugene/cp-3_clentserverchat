import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import { history } from './history';

export const getStoreReducers = (injectedReducers = {}) => {
  return combineReducers({
    router: connectRouter(history),
    ...injectedReducers,
  });
};
