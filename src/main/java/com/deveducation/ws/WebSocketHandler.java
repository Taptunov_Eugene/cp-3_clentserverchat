package com.deveducation.ws;

import com.deveducation.Application;
import com.google.gson.Gson;
import com.deveducation.dto.Message;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.websocketx.*;
import com.deveducation.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebSocketHandler extends ChannelInboundHandlerAdapter {

    public static final Logger logger = LoggerFactory.getLogger(WebSocketHandler.class);
    private final Gson gson = new Gson();

    @Override
    public void channelRead(ChannelHandlerContext context, Object msg) {

        if (msg instanceof WebSocketFrame) {

            logger.info("Client Channel : " + context.channel());
            if (msg instanceof BinaryWebSocketFrame) {
                logger.info("BinaryWebSocketFrame Received : " + ((BinaryWebSocketFrame) msg).content());
            } else if (msg instanceof TextWebSocketFrame) {

                Message message = gson.fromJson(((TextWebSocketFrame) msg).text(), Message.class);
                String response = MessageService.commandHandle(message);
                context.channel().writeAndFlush(new TextWebSocketFrame(response));

                logger.info(((TextWebSocketFrame) msg).text());
            } else if (msg instanceof PingWebSocketFrame) {
                logger.info("PingWebSocketFrame Received : " + ((PingWebSocketFrame) msg).content());

            } else if (msg instanceof PongWebSocketFrame) {
                logger.info("PongWebSocketFrame Received : " + ((PongWebSocketFrame) msg).content());
            } else if (msg instanceof CloseWebSocketFrame) {
                logger.info("CloseWebSocketFrame Received : ");
                logger.info("ReasonText :" + ((CloseWebSocketFrame) msg).reasonText());
                logger.info("StatusCode : " + ((CloseWebSocketFrame) msg).statusCode());
            } else {
                logger.warn("Unsupported WebSocketFrame");
            }
        }
    }
}
