package com.deveducation.ws;

import com.deveducation.dto.RegisterMessage;
import com.deveducation.utils.RegUtil;
import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import com.deveducation.dto.LoginMessage;
import com.deveducation.dto.TokenResponse;
import com.deveducation.jwtToken.TokenGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

public class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private final Gson gson = new Gson();

    public static final Logger logger = LoggerFactory.getLogger(HttpServerHandler.class);
    private static final String WS = "/ws?token=";
    private static final String AUTH = "/login";
    private static final String REGISTRATION = "/registration";
    private static final String PEOPLE = "/people";
    private static final String CHANNELS = "/channels";

    WebSocketServerHandshaker handShaker;

    @Override
    public void channelRead0(ChannelHandlerContext context, FullHttpRequest httpRequest) {
        String uri = httpRequest.uri();
        if (uri.startsWith(WS)) {
            String substring = uri.substring(uri.indexOf("=") + 1);
            HttpHeaders headers = httpRequest.headers();

            if (TokenGenerator.isTokenValid(substring)) {
                logger.info("Connection : " + headers.get("Connection"));
                logger.info("Upgrade : " + headers.get("Upgrade"));
                if ("Upgrade".equalsIgnoreCase(headers.get(HttpHeaderNames.CONNECTION)) &&
                        "WebSocket".equalsIgnoreCase(headers.get(HttpHeaderNames.UPGRADE))) {

                    //Adding new handler to the existing pipeline to handle WebSocket Messages
                    context.pipeline().replace(this, "websocketHandler", new WebSocketHandler());
                    logger.info("WebSocketHandler added to the pipeline");
                    logger.info("Opened Channel : " + context.channel());
                    logger.info("Handshaking....");
                    //Do the Handshake to upgrade connection from HTTP to WebSocket protocol
                    handleHandshake(context, httpRequest);
                    logger.info("Handshake is done");
                }
            } else {
                badResponseGenerate(context);
                logger.warn("Bad request");
            }
        } else if (uri.startsWith(AUTH)) {
            LoginMessage loginMessage = new Gson().fromJson(httpRequest.content().toString(StandardCharsets.UTF_8), LoginMessage.class);
            String token = null;
            try {
                token = TokenGenerator.generateToken(loginMessage.getEmail(), loginMessage.getPassword());
            } catch (IllegalArgumentException e) {
                badResponseGenerate(context);
                logger.error("Authorization failed");
            }
            String json = gson.toJson(new TokenResponse(new TokenResponse.Tokens(token, token), "12"));

            ByteBuf responseBytes = context.alloc().buffer();
            responseBytes.writeBytes(json.getBytes());
            successResponseGenerate(context, responseBytes);

        } else if (uri.startsWith(REGISTRATION)) {
            ByteBuf responseBytes = context.alloc().buffer();
            RegisterMessage registration = new Gson().fromJson(httpRequest.content().toString(StandardCharsets.UTF_8), RegisterMessage.class);
            boolean isUserAlreadyRegistered = RegUtil.checkPersonByEmail(registration.getEmail());
            if (!isUserAlreadyRegistered) {
                boolean isUserRegistered = RegUtil.createPerson(registration.getUserName(), registration.getEmail(), registration.getPassword());
                if (isUserRegistered) {
                    successResponseGenerate(context, responseBytes);
                    logger.info("New user was registered");
                } else {
                    badResponseGenerate(context);
                    logger.warn("Such user is already registered");
                }
            } else {
                badResponseGenerate(context);
                logger.warn("Such user is already registered");
            }
        } else if (uri.startsWith(PEOPLE)) {

        }
    }

    private void successResponseGenerate(ChannelHandlerContext context, ByteBuf responseBytes) {
        FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, responseBytes);
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_TYPE, "application/json");
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_LENGTH, httpResponse.content().readableBytes());
        httpResponse.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);

        context.channel().writeAndFlush(httpResponse);
    }

    private void badResponseGenerate(ChannelHandlerContext context) {
        FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST);
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_TYPE, "application/json");
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_LENGTH, httpResponse.content().readableBytes());
        httpResponse.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);

        context.channel().writeAndFlush(httpResponse);
    }

    /* Do the handshaking for WebSocket request */
    protected void handleHandshake(ChannelHandlerContext context, HttpRequest request) {
        WebSocketServerHandshakerFactory wsFactory =
                new WebSocketServerHandshakerFactory(getWebSocketURL(request), null, true);
        handShaker = wsFactory.newHandshaker(request);
        if (handShaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(context.channel());
        } else {
            handShaker.handshake(context.channel(), request);
        }
    }

    protected String getWebSocketURL(HttpRequest request) {
        logger.info("Request URI : " + request.getUri());
        String url = "ws://" + request.headers().get("Host") + request.getUri();
        logger.info("Constructed URL : " + url);
        return url;
    }
}
