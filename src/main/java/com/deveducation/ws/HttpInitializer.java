package com.deveducation.ws;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.cors.CorsConfig;
import io.netty.handler.codec.http.cors.CorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpInitializer extends ChannelInitializer<NioSocketChannel> {

    public static final Logger logger = LoggerFactory.getLogger(HttpInitializer.class);

    @Override
    protected void initChannel(NioSocketChannel nioSocketChannel) {
        CorsConfig.Builder builder = new CorsConfig.Builder();

        ChannelPipeline pipeline = nioSocketChannel.pipeline();
        pipeline.addLast(new HttpServerCodec());
        pipeline.addLast(new HttpObjectAggregator(1048576));
        pipeline.addLast(new CorsHandler(builder.build()));
        pipeline.addLast(new HttpServerHandler());
    }
}