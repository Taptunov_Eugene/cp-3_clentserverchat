package com.deveducation.toJson;

public class ChatJson {

    private int id_chat;
    private String name;

    public ChatJson(int id_chat, String name) {
        this.id_chat = id_chat;
        this.name = name;
    }

    public int getId_chat() {
        return id_chat;
    }

    public void setId_chat(int id_chat) {
        this.id_chat = id_chat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
