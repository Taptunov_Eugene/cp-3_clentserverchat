package com.deveducation.toJson;

public class MessageJson {

    private int id_message;
    private String text;
    private String nameOfChat;
    private String time;
    private String user;

    public MessageJson(int id_message, String text, String nameOfChat, String time, String user) {
        this.id_message = id_message;
        this.text = text;
        this.nameOfChat = nameOfChat;
        this.time = time;
        this.user = user;
    }

    public int getId_message() {
        return id_message;
    }

    public void setId_message(int id_message) {
        this.id_message = id_message;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNameOfChat() {
        return nameOfChat;
    }

    public void setNameOfChat(String nameOfChat) {
        this.nameOfChat = nameOfChat;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
