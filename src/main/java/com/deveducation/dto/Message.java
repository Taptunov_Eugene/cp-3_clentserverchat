package com.deveducation.dto;

public class Message {
    private Command command;
    private String text;
    private Params params;
    private long userId;

    public Command getCommand() {
        return command;
    }

    public String getText() {
        return text;
    }

    public Params getParams() {
        return params;
    }

    public long getUserId() {
        return userId;
    }
}
