package com.deveducation.dto;

public class Params {

    //registration command
    private String firstName;
    private String lastName;
    private String urlPhoto;
    private String userName;
    private String password;

    //create chat command
    private String chatName;

    //send message command
    private long sendTime;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getChatName() {
        return chatName;
    }

    public long getSendTime() {
        return sendTime;
    }
}
