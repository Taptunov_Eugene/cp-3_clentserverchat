package com.deveducation.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class User {
    @JsonProperty("_id")
    String id;

    @JsonProperty("username")
    String userName;

    @JsonProperty("email")
    String email;

    @JsonProperty("name")
    String name;

    public User(String id, String userName, String email, String name) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }
}
