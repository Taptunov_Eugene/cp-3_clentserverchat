package com.deveducation.dto;

public enum Command {
    SEND_MESSAGE,
    LOGIN,
    LOGOUT,
    CREATE_CHAT,
    REMOVE_CHAT,
    REGISTER_USER,
    START_GAME,
    STOP_GAME,
    CURRENT_USER,
    USERS,
    ROOMS
}
