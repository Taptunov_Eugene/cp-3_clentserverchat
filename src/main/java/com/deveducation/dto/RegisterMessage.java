package com.deveducation.dto;

public class RegisterMessage {
    String email;
    String password;
    String userName;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }
}
