package com.deveducation.model;

import java.util.List;

public class Room {
    private int id;
    private String name;
    private List<Integer> messagesId;
    private List<Integer> usersId;

    public Room() {
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addMessage(int id) {
        this.messagesId.add(id);
    }

    public List<Integer> getMessagesId() {
        return this.messagesId;
    }

    public List<Integer> getUsersId() {
        return usersId;
    }

    public void addUserId(int id) {
        this.usersId.add(id);
    }

    public void setUsersId(List<Integer> usersId) {
        this.usersId = usersId;
    }

    public Room(int id, String name, List<Integer> messagesId, List<Integer> usersId) {
        this.id = id;
        this.name = name;
        this.messagesId = messagesId;
        this.usersId = usersId;
    }

    @Override
    public String toString() {
        final StringBuffer stringBuffer = new StringBuffer("Room{");
        stringBuffer.append("id=").append(id);
        stringBuffer.append(", name='").append(name).append('\'');
        stringBuffer.append(", messagesId=").append(messagesId);
        stringBuffer.append(", usersId=").append(usersId);
        stringBuffer.append('}');
        return stringBuffer.toString();
    }
}
