package com.deveducation.model;

public class Message {
    private int id;
    private String text;
    private long userId;
    private int roomId;
    private String dataTime;

    public Message(int id, String text, long userId, int roomId, String dataTime) {
        this.id = id;
        this.text = text;
        this.userId = userId;
        this.roomId = roomId;
        this.dataTime = dataTime;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setText(String text) {
        this.text = text;
    }

    public String getDataTime() {
        return dataTime;
    }

    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @Override
    public String toString() {
        final StringBuffer stringBuffer = new StringBuffer("Message{");
        stringBuffer.append("id=").append(id);
        stringBuffer.append(", text='").append(text).append('\'');
        stringBuffer.append(", userId=").append(userId);
        stringBuffer.append(", roomId=").append(roomId);
        stringBuffer.append(", dataTime='").append(dataTime).append('\'');
        stringBuffer.append('}');
        return stringBuffer.toString();
    }
}
