package com.deveducation.utils;

import com.deveducation.db.Dao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

import static java.lang.Integer.parseInt;

public class RegUtil {

    public static final Logger logger = LoggerFactory.getLogger(RegUtil.class);

    public static boolean createPerson(String firstName, String login, String password) {
        try {
            PreparedStatement statement = Dao.getConnection().prepareStatement(
                    "INSERT INTO users" +
                            "(firstname, surName, login, password, roomsId, photoUrl, statusonline)" +
                            "VALUES ( ?, ?, ?, ?, ?,  ?,?) "
            );
            statement.setString(1, firstName);
            statement.setString(2, "1");
            statement.setString(3, login);
            statement.setString(4, password);
            statement.setString(5, "id");
            statement.setString(6, "account.getPhotoUrl()");
            statement.setBoolean(7, true);
            statement.execute();
            logger.info("Create: " + login);

            return true;
        } catch (SQLException e) {
            logger.error("Can't create new user");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkPerson(String login, String password) {
        try {
            ObservableList<Integer> listOfPersons = FXCollections.observableArrayList();
            PreparedStatement preparedStatement =
                    Dao.getConnection().prepareStatement(
                            "SELECT * FROM users WHERE login =" + "'" + login + "'" + " AND password =" + "'" + password + "'"
                    );
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = parseInt(resultSet.getString("id"));

                listOfPersons.add(id);
            }
            if (listOfPersons.size() == 0) {
                return false;
            }
            return true;

        } catch (SQLException e) {
            logger.error("Can't find the user");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkPersonByEmail(String email) {
        try {
            ObservableList<Integer> listOfPersons = FXCollections.observableArrayList();
            PreparedStatement preparedStatement =
                    Dao.getConnection().prepareStatement(
                            "SELECT * FROM users WHERE login =" + "'" + email + "'"
                    );
            try (ResultSet query = preparedStatement.executeQuery()) {
                while (query.next()) {
                    int id = parseInt(query.getString("id"));
                    listOfPersons.add(id);
                }
            }
            if (listOfPersons.size() == 0) {
                return false;
            }
            return true;

        } catch (SQLException e) {
            logger.error("Can't find the user");
            e.printStackTrace();
            return false;
        }
    }
}
