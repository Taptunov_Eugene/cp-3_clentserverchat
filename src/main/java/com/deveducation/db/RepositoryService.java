package com.deveducation.db;

import com.deveducation.utils.RegUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class RepositoryService {

    public static final Logger logger = LoggerFactory.getLogger(RepositoryService.class);

    public static boolean verifyUser(String login, String password) {
        logger.info("User was verified");
        return RegUtil.checkPerson(login, password);
//        return login.equals("dasha") ? password.equals("petya"): login.equals("vasya");
    }

    public static boolean verifyUserSignup(String username, String login, String password) {
        logger.info("User was registered");
        return RegUtil.createPerson(username, login, password);
    }
}
