package com.deveducation.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class Dao {

    public static final Logger logger = LoggerFactory.getLogger(Dao.class);
    private static Statement statement;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    public static Statement getStatement() {
        return statement;
    }

    public static void setStatement(Statement statement) {
        Dao.statement = statement;
    }

    public static PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    public static void setPreparedStatement(PreparedStatement preparedStatement) {
        Dao.preparedStatement = preparedStatement;
    }

    public static ResultSet getResultSet() {
        return resultSet;
    }

    public static void setResultSet(ResultSet resultSet) {
        Dao.resultSet = resultSet;
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void setConnection(Connection connection) {
        Dao.connection = connection;
    }

    private static Connection connection = null;

    public static void PostgreRepository() {
        logger.info("-------- PostgreSQL "
                + "JDBC Connection Testing ------------");
        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {
            logger.error("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();
        }
        logger.info("PostgreSQL JDBC Driver Registered!");
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://ec2-3-248-4-172.eu-west-1.compute.amazonaws.com:5432/d53knvbfufupa3", "gnqzinvidwoiqa",
                    "3b966ca1a99d7a2f0e97131be7bd08862b343ef02467ced66ad98e855076db34");
            statement = connection.createStatement();
        } catch (SQLException e) {
            logger.error("Connection Failed! Check output console");
            e.printStackTrace();
        }
        if (connection != null) {
            logger.info("You made it, take control your database now!");
        } else {
            logger.warn("Failed to make connection!");
        }

//        try {
//            String myTableName = "CREATE TABLE users("
//                    + "id   SERIAL PRIMARY KEY,"
//                    + "firstName CHARACTER VARYING(30)     NOT NULL,"
//                    + "surName CHARACTER VARYING(30) NOT NULL,"
//                    + "login CHARACTER VARYING(30) NOT NULL,"
//                    + "password CHARACTER VARYING(30) NOT NULL,"
//                    + "roomsId CHARACTER VARYING(30) NOT NULL,"
//                    + "photoUrl CHARACTER VARYING(30) NOT NULL,"
//                    + "statusOnline CHARACTER VARYING(30) NOT NULL;";
//            statement.executeUpdate(myTableName);
//            logger.info("Table created");
//        } catch (SQLException e) {
//            logger.error("Can't create table");
//            e.printStackTrace();
//        }
    }
}

